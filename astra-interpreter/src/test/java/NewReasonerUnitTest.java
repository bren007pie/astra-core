import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.junit.BeforeClass;
import org.junit.Test;

import astra.formula.AND;
import astra.formula.BracketFormula;
import astra.formula.Comparison;
import astra.formula.Formula;
import astra.formula.Inference;
import astra.formula.NOT;
import astra.formula.OR;
import astra.formula.Predicate;
import astra.reasoner.NewReasoner;
import astra.reasoner.Queryable;
import astra.reasoner.Reasoner;
import astra.term.Funct;
import astra.term.Primitive;
import astra.term.Term;
import astra.term.Variable;
import astra.type.Type;

public class NewReasonerUnitTest {
    static Reasoner reasoner;
    static AND complexAnd = new AND(
        new Predicate("has", new Term[] { new Variable(Type.STRING, "X") }),
        new AND(
            new Predicate("on", new Term[] { new Variable(Type.STRING, "X"), new Variable(Type.STRING, "Y") }),
            new Predicate("on", new Term[] { new Variable(Type.STRING, "X"), new Variable(Type.STRING, "Z") })
            )
    );

    @BeforeClass
    public static void setup() {
        reasoner = new NewReasoner(null);
        reasoner.addSource(new Queryable() {
            List<Formula> list = new LinkedList<>();

            {
                list.add(new Predicate("has", new Term[] { Primitive.newPrimitive("x") }));
                list.add(new Predicate("on", new Term[] { Primitive.newPrimitive("x"), Primitive.newPrimitive("a") }));
                list.add(new Predicate("on", new Term[] { Primitive.newPrimitive("a"), Primitive.newPrimitive("b") }));
                list.add(new Predicate("on",
                        new Term[] { Primitive.newPrimitive("b"), Primitive.newPrimitive("table") }));
                list.add(new Predicate("on",
                        new Term[] { Primitive.newPrimitive("c"), Primitive.newPrimitive("table") }));
                list.add(
                    new Inference(
                        new Predicate("on", new Term[] { new Variable(Type.STRING, "X"), new Variable(Type.STRING, "Z") }),
                        complexAnd
                ));
            }

            @Override
            public void addMatchingFormulae(Queue<Formula> queue, Formula predicate) {
                queue.addAll(list);

            }

            @Override
            public Iterator<Formula> iterator(Formula target) {
                return list.iterator();
            }
        });

    }

    @Test
    public void queryGroundPredicate() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner.query(
                new Predicate("on", new Term[] { Primitive.newPrimitive("rem"), Primitive.newPrimitive("fire") }));
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNull(bindings);
    }

    @Test
    public void queryPartialPredicate() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner
                .query(new Predicate("on", new Term[] { Primitive.newPrimitive("a"), new Variable(Type.STRING, "X") }));
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNotNull(bindings);
    }

    @Test
    public void queryNOTPartialPredicate() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner.query(new NOT(
                new Predicate("on", new Term[] { Primitive.newPrimitive("a"), new Variable(Type.STRING, "X") })));
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNull(bindings);
    }

    @Test
    public void queryNOT() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner.query(new NOT(
                new Predicate("on", new Term[] { Primitive.newPrimitive("rem"), Primitive.newPrimitive("fire") })));
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNotNull(bindings);
    }

    @Test
    public void queryOnePredicate() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner.query(
                new Predicate("on", new Term[] { new Variable(Type.STRING, "X"), new Variable(Type.STRING, "Y") }));
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNotNull(bindings);
    }

    @Test
    public void queryOneFailPredicate() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner.query(
                new Predicate("won", new Term[] { new Variable(Type.STRING, "X"), new Variable(Type.STRING, "Y") }));
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNull(bindings);
    }

    @Test
    public void queryAllPredicate() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner.queryAll(
                new Predicate("on", new Term[] { new Variable(Type.STRING, "X"), new Variable(Type.STRING, "Y") }));
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNotNull(bindings);
    }

    @Test
    public void queryAllComplexAND() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner.queryAll(complexAnd);
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNotNull(bindings);
    }

    @Test
    public void queryAND() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner.query(new AND(
                new Predicate("on", new Term[] { new Variable(Type.STRING, "X"), new Variable(Type.STRING, "Y") }),
                new Predicate("on", new Term[] { new Variable(Type.STRING, "Y"), new Variable(Type.STRING, "Z") })));
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNotNull(bindings);
    }

    @Test
    public void queryAllAND() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner.query(new AND(
                new Predicate("on", new Term[] { new Variable(Type.STRING, "X"), new Variable(Type.STRING, "Y") }),
                new Predicate("on", new Term[] { new Variable(Type.STRING, "Y"), new Variable(Type.STRING, "Z") })));
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNotNull(bindings);
    }

    @Test
    public void queryANDNOT() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner.query(new AND(
                new Predicate("on", new Term[] { new Variable(Type.STRING, "X"), new Variable(Type.STRING, "Y") }),
                new NOT(new Predicate("on",
                        new Term[] { new Variable(Type.STRING, "Y"), new Variable(Type.STRING, "Z") }))));
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNotNull(bindings);
    }

    @Test
    public void queryAllANDNOT() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner.queryAll(new AND(
                new Predicate("on", new Term[] { new Variable(Type.STRING, "X"), new Variable(Type.STRING, "Y") }),
                new NOT(new Predicate("on",
                        new Term[] { new Variable(Type.STRING, "Y"), new Variable(Type.STRING, "Z") }))));
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNotNull(bindings);
    }

    @Test
    public void querySimpleBracket() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner.query(new BracketFormula(
                new Predicate("on", new Term[] { Primitive.newPrimitive("a"), new Variable(Type.STRING, "Y") })));
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNotNull(bindings);
    }

    @Test
    public void querySimpleFailBracket() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner.query(new BracketFormula(
                new Predicate("on", new Term[] { Primitive.newPrimitive("rem"), Primitive.newPrimitive("fire") })));
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNull(bindings);
    }

    @Test
    public void queryANDBracket() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner.query(new BracketFormula(new AND(
                new Predicate("on", new Term[] { new Variable(Type.STRING, "X"), new Variable(Type.STRING, "Y") }),
                new NOT(new Predicate("on",
                        new Term[] { new Variable(Type.STRING, "Y"), new Variable(Type.STRING, "Z") })))));
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNotNull(bindings);
    }

    @Test
    public void queryComparison() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner
                .query(new Comparison(Comparison.GREATER_THAN, Primitive.newPrimitive(2), Primitive.newPrimitive(1)));
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNotNull(bindings);
    }

    @Test
    public void queryComparisonFail() {
        long start = System.currentTimeMillis();
        List<Map<Integer, Term>> bindings = reasoner
                .query(new Comparison(Comparison.GREATER_THAN, Primitive.newPrimitive(1), Primitive.newPrimitive(2)));
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNull(bindings);
    }

    // ADD TESTS FOR query(Formula, Bindings)************
    @Test
    public void queryANDComparison() {
        long start = System.currentTimeMillis();
        Map<Integer, Term> initial = new HashMap<>();
        Variable var = new Variable(Type.INTEGER, "N");

        initial.put(var.id(), Primitive.newPrimitive(3));
        List<Map<Integer, Term>> bindings = reasoner.query(
                new AND(new Predicate("on", new Term[] { Primitive.newPrimitive("a"), new Variable(Type.STRING, "Y") }),
                        new BracketFormula(new Comparison(Comparison.GREATER_THAN, new Variable(Type.INTEGER, "N"),
                                Primitive.newPrimitive(1)))),
                initial);
        System.out.println("duration: " + (System.currentTimeMillis() - start));
        System.out.println(bindings);
        assertNotNull(bindings);
    }

    @Test
    public void noBeliefsQueryTest() {
        Reasoner reasoner = new NewReasoner(null);
        List<Map<Integer, Term>> bindings = reasoner.query(new Predicate("checked", new Term[] {
            new Funct("test", new Term[0])
        }));
        System.out.println("bindings="+bindings);
    }

    @Test
    public void orFirstPassQueryTest() {
        List<Map<Integer, Term>> bindings = reasoner.query(new OR(
            new Predicate("on", new Term[] { Primitive.newPrimitive("x"), new Variable(Type.STRING, "X") }),
            new Predicate("on", new Term[] { Primitive.newPrimitive("a"), new Variable(Type.STRING, "X") })
        ));
        System.out.println("bindings="+bindings);
    }

    @Test
    public void orSecondPassQueryTest() {
        List<Map<Integer, Term>> bindings = reasoner.query(new OR(
            new Predicate("on", new Term[] { Primitive.newPrimitive("g"), new Variable(Type.STRING, "X") }),
            new Predicate("on", new Term[] { Primitive.newPrimitive("a"), new Variable(Type.STRING, "X") })
        ));
        System.out.println("bindings="+bindings);
    }

    @Test
    public void orNoPassQueryTest() {
        List<Map<Integer, Term>> bindings = reasoner.query(new OR(
            new Predicate("on", new Term[] { Primitive.newPrimitive("g"), new Variable(Type.STRING, "X") }),
            new Predicate("on", new Term[] { Primitive.newPrimitive("v"), new Variable(Type.STRING, "X") })
        ));
        System.out.println("bindings="+bindings);
    }

    @Test
    public void orBracketLeftPassQueryTest() {
        List<Map<Integer, Term>> bindings = reasoner.query(new BracketFormula(new OR(
            new Predicate("on", new Term[] { Primitive.newPrimitive("x"), new Variable(Type.STRING, "X") }),
            new Predicate("on", new Term[] { Primitive.newPrimitive("v"), new Variable(Type.STRING, "X") })
        )));
        System.out.println("bindings="+bindings);
    }
    
    @Test
    public void orBracketRightPassQueryTest() {
        List<Map<Integer, Term>> bindings = reasoner.query(new BracketFormula(new OR(
            new Predicate("on", new Term[] { Primitive.newPrimitive("g"), new Variable(Type.STRING, "X") }),
            new Predicate("on", new Term[] { Primitive.newPrimitive("x"), new Variable(Type.STRING, "X") })
        )));
        System.out.println("bindings="+bindings);
        assertNotNull(bindings);
    }
    
    @Test
    public void orBracketNoPassQueryTest() {
        List<Map<Integer, Term>> bindings = reasoner.query(new BracketFormula(new OR(
            new Predicate("on", new Term[] { Primitive.newPrimitive("g"), new Variable(Type.STRING, "X") }),
            new Predicate("on", new Term[] { Primitive.newPrimitive("v"), new Variable(Type.STRING, "X") })
        )));
        System.out.println("bindings="+bindings);
        assertNull(bindings);
    }

    @Test
    public void trueBracketQueryTest() {
        List<Map<Integer, Term>> bindings = reasoner.query(new BracketFormula(Predicate.TRUE));
        System.out.println("bindings="+bindings);
        assertNotNull(bindings);
    }

    @Test
    public void falseBracketQueryTest() {
        List<Map<Integer, Term>> bindings = reasoner.query(new BracketFormula(Predicate.FALSE));
        System.out.println("bindings="+bindings);
        assertNull(bindings);
    }

    @Test
    public void andTrueBracketQueryTest() {
        List<Map<Integer, Term>> bindings = reasoner.query(new BracketFormula(new AND(
            new BracketFormula(Predicate.TRUE),
            new BracketFormula(Predicate.TRUE))
        ));
        System.out.println("bindings="+bindings);
        assertNotNull(bindings);
    }

    @Test
    public void andTrueNoBracketQueryTest() {
        List<Map<Integer, Term>> bindings = reasoner.query(new BracketFormula(new AND(
            new BracketFormula(Predicate.TRUE),
            Predicate.FALSE
        )));
        System.out.println("bindings="+bindings);
        assertNull(bindings);
    }

}
