package astra.type;

public class ObjectType extends Type {
	/**
	 *
	 */
	private static final long serialVersionUID = 5614219026092151646L;
	Class<?> subtype;
	
	public ObjectType(Class<?> subtype) {
		super("object");
		this.subtype = subtype;
	}

	public Class<?> subtype() {
		return subtype;
	}
}
