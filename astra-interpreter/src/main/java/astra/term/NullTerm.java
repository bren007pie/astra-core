package astra.term;

import astra.reasoner.util.LogicVisitor;
import astra.type.Type;

public class NullTerm implements Term {

	/**
	 *
	 */
	private static final long serialVersionUID = -6673541855097777067L;

	@Override
	public Type type() {
		return null;
	}

	@Override
	public Object accept(LogicVisitor visitor) {
		return null;
	}

	@Override
	public boolean matches(Term right) {
		return false;
	}

	public boolean equals(Object object) {
		return (object instanceof NullTerm);
	}

	@Override
	public String signature() {
		return null;
	}
	
	public NullTerm clone() {
		return this;
	}

}
