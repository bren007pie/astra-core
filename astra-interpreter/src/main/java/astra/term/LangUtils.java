package astra.term;

public class LangUtils {
    public static Object[] toArray(Term[] terms) {
        Object[] array = new Object[terms.length];
        for (int i=0; i<terms.length;i++) {
            if (terms[i] instanceof Primitive) {
                array[i] = ((Primitive<?>) terms[i]).value();
            } else {
                System.out.println("Could not convert: " + terms[i] + " to an object...");
                System.exit(1);
            }
        }
        return array;
    }
    
}
