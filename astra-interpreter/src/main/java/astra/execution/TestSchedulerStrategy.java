package astra.execution;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import astra.core.Agent;
import astra.core.Task;

/**
 * Strategy:
 * 
 * Agent added gets set to "WAITING" state Agents executed get set to "ACTIVE"
 * state Agents suspended get set to a "SUSPENDED" state
 * 
 */
public class TestSchedulerStrategy implements SchedulerStrategy {
    Map<String, Integer> counts = new HashMap<>();
    boolean finished = false;
    ExecutorService service;

    public TestSchedulerStrategy() {
        int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("[Scheduler] Processors: " + processors);
        service = Executors.newFixedThreadPool(processors);
    }

    @Override
    public void schedule(Agent agent) {
        service.execute(() -> {
            try {
                agent.execute();
            } catch (Throwable th) {
                th.printStackTrace();
            }
            // counts.put(agent.name(), counts.getOrDefault(agent.name(), 0) + 1);
            if (agent.isActive()) {
                if (!finished) {
                    schedule(agent);
                }
            // } else {
            //     Agent.out.println(agent.name() + ",SUSPENDING");
            }            
        });
    }

    @Override
    public void schedule(Task task) {
        // executor.submit(task);
    }

    public String toString() {
        // return counts.toString();
        return "CLOSED";
    }

    @Override
    public void setThreadPoolSize(int size) {
    }

    @Override
    public void stop() {
    }

    @Override
    public void setState(Agent agent, int state) {
    }

    @Override
    public int getState(Agent agent) {
        return 0;
    }

    @Override
    public void setSleepTime(long sleepTime) {
    }

    @Override
    public void shutdown() {
        finished = true;
    }
    
}
