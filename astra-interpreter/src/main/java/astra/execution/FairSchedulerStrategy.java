package astra.execution;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import astra.core.Agent;
import astra.core.Task;

/**
 * Strategy:
 * 
 * Agent added gets set to "WAITING" state Agents executed get set to "ACTIVE"
 * state Agents suspended get set to a "SUSPENDED" state
 * 
 */
public class FairSchedulerStrategy implements SchedulerStrategy {
    List<Agent> list = new CopyOnWriteArrayList<>();
    Map<String, Integer> counts = new HashMap<>();
    boolean finished = false;
    public FairSchedulerStrategy() {
        new Thread() {
            public void run() {
                while (!finished) {
                    list.parallelStream().forEach(agent -> {
                        counts.put(agent.name(), counts.get(agent.name()) + 1);
                        agent.execute();
                    });
                }
            }
        }.start();
    }

    @Override
    public void schedule(Agent agent) {
        counts.put(agent.name(), 0);
        list.add(agent);
    }

    @Override
    public void schedule(Task task) {
        // executor.submit(task);
    }

    public String toString() {
        return counts.toString();
    }

    @Override
    public void setThreadPoolSize(int size) {
    }

    @Override
    public void stop() {
    }

    @Override
    public void setState(Agent agent, int state) {
    }

    @Override
    public int getState(Agent agent) {
        return 0;
    }

    @Override
    public void setSleepTime(long sleepTime) {
    }

    @Override
    public void shutdown() {
        finished = true;
        list.clear();
    }
}
