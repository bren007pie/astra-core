package astra.core;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import astra.formula.Formula;
import astra.term.Primitive;
import astra.term.Term;

public class Fragment {
	private Map<String, Module> modules = new HashMap<String, Module>();
	ASTRAClass clazz;
	Fragment next;
	List<ASTRAClass> linearization;
	
	
	public Fragment(ASTRAClass clazz) throws ASTRAClassNotFoundException {
		this.clazz = clazz;
		linearization = clazz.getLinearization();
	}

	public List<ASTRAClass> getLinearization() {
		return linearization;
	}

	public void addModule(String name, Class<?> cls, Agent agent) {
		try {
			Module module = (Module) cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
			module.setAgent(agent);
			modules.put(name,  module);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

	public void addModule(String name, Class<?> cls, Term[] params, Agent agent) {
		Object[] values = getValues(params);
		try {
			Module module = (Module) cls.getDeclaredConstructor(getTypes(values)).newInstance(values);
			module.setAgent(agent);
			modules.put(name,  module);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private Object[] getValues(Term[] params) {
		Object[] values = new Object[params.length];
		for (int i=0; i<params.length; i++) {
			if (params[i] instanceof Primitive) {
				values[i] = ((Primitive<?>) params[i]).value();
			} else {
				throw new RuntimeException("Invalid type in module constructor: " + params[i]);
			}
		}
		return values;
	}
	private Class<?>[] getTypes(Object[] values) {
		Class<?>[] classes = new Class[values.length];
		for (int i=0; i<values.length; i++) {
			classes[i] = values[i].getClass();
		}
		return classes;
	}
	public void addModule(String name, String urn, Agent agent) {
		try {
			Module module = (Module) Class.forName(urn).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
			module.setAgent(agent);
			modules.put(name,  module);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

	public ASTRAClass getASTRAClass() {
		return clazz;
	}

	public Module getModule(String key) {
		return modules.get(key);
	}

	public List<Formula> getMatchingFormulae(Formula predicate) {
		return null;
	}
	
}
