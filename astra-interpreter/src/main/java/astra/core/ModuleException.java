package astra.core;

public class ModuleException extends RuntimeException {
	/**
	 *
	 */
	private static final long serialVersionUID = -871256592107931612L;

	public ModuleException(Exception e) {
		super(e.getMessage());
	}

	public ModuleException(String msg, Exception e) {
		super(msg, e);
	}
	
	public ModuleException(String msg) {
		super(msg);
	}

}
