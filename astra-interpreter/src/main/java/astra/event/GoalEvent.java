package astra.event;

import astra.formula.Goal;
import astra.reasoner.util.LogicVisitor;

public class GoalEvent implements Event {
	public char type;
	public Goal goal;
	public Object source;

	public GoalEvent(char type, Goal goal) {
		this(type, goal, null);
	}

	public GoalEvent(char type, Goal goal, Object source) {
		this.type = type;
		this.goal = goal;
		this.source = source;
	}

	public char type() {
		return type;
	}

	public Goal goal() {
		return goal;
	}

	public String toString() {
		return type + goal.toString();
	}

	public Object getSource() {
		return source;
	}

	public String signature() {
		return "GE:" + type + ":" + goal.formula().id() + ":" + goal.formula().terms().length;
	}

	@Override
	public Event accept(LogicVisitor visitor) {
		return new GoalEvent(type, (Goal) goal.accept(visitor), source);
	}
}
