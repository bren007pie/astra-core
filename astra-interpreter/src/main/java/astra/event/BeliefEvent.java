package astra.event;

import astra.formula.Predicate;
import astra.reasoner.util.LogicVisitor;

public class BeliefEvent implements Event {
	public char type;
	public Predicate belief;

	public BeliefEvent(char type, Predicate belief) {
		this.type = type;
		this.belief = belief;
	}

	public char type() {
		return type;
	}

	public Predicate belief() {
		return belief;
	}

	public Object getSource() {
		return null;
	}

	public String toString() {
		return type + belief.toString();
	}

	public String signature() {
		return "BE:" + type + ":" + belief.id() + ":" + belief.terms().length;
	}

	@Override
	public Event accept(LogicVisitor visitor) {
		return new BeliefEvent(type, (Predicate) belief.accept(visitor));
	}
}
