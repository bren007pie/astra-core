package astra.statement;

import astra.core.Intention;

public class TryRecover extends AbstractStatement {
	Statement tryStatement, recoverStatement;
	
	public TryRecover(String clazz, int[] data, Statement ifStatement, Statement elseStatement) {
		this.setLocation(clazz, data[0], data[1], data[2], data[3]);
		this.tryStatement = ifStatement;
		this.recoverStatement = elseStatement;
	}
	
	@Override
	public StatementHandler getStatementHandler() {
		return new AbstractStatementHandler() {
			int state = 0;
			@Override
			public boolean execute(Intention intention) {
				switch(state) {
				case 0:
					executor.addStatement(tryStatement.getStatementHandler());
					state = 2;
					break;
				case 1:
					executor.addStatement(recoverStatement.getStatementHandler());
					state = 3;
					break;
				case 2:
				case 3:
					return false;
				}
				
				return true;
			}

			@Override
			public boolean onFail(Intention context) {
				// Failure in try - switch to recovery plan
				if (state == 2) {
					state = 1;
					return true;
				}
				
				// This reflects failure of the recovery plan
				if (state == 3) return false;
				
				// default return value - failure handled here...
				return true;
			}

			@Override
			public Statement statement() {
				return TryRecover.this;
			}
			
		};
	}

}
