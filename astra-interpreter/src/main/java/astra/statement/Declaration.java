package astra.statement;

import astra.core.Intention;
import astra.reasoner.util.ContextEvaluateVisitor;
import astra.term.Term;
import astra.term.Variable;

public class Declaration extends AbstractStatement {
	Variable variable;
	Term value;
	
	public Declaration(Variable variable, Term value) {
		this.variable = variable;
		this.value = value;
	}

	public Declaration(Variable variable, String clazz, int[] data, Term value) {
		setLocation(clazz, data[0], data[1], data[2], data[3]);
		this.variable = variable;
		this.value = value;
	}

	public Declaration(Variable variable) {
		this.variable = variable;
	}

	public Declaration(Variable variable, String clazz, int[] data) {
		setLocation(clazz, data[0], data[1], data[2], data[3]);
		this.variable = variable;
	}

	@Override
	public StatementHandler getStatementHandler() {
		return new AbstractStatementHandler() {

			@Override
			public boolean execute(Intention intention) {
				// System.out.println("Declaration: " + variable);
				if (value == null) {
					executor.addVariable(variable);
				} else {
					// System.out.println("value:" + value.getClass().getCanonicalName());
					try {
						executor.addVariable(variable, (Term) value.accept(new ContextEvaluateVisitor(intention, true)));
					} catch (Throwable e) {
						intention.failed("Variable Declaration Failed: " + variable, e);
						return false;
					}
				}
				
				return false;
			}

			@Override
			public boolean onFail(Intention context) {
				return false;
			}

			@Override
			public Statement statement() {
				return Declaration.this;
			}
			
			public String toString() {
				return variable.type() + " " + variable + ((value == null) ? "" : " = " + value);
			}
			
		};
	}
}
