package astra.statement;

import java.util.Map;

import astra.core.Intention;
import astra.formula.Formula;
import astra.reasoner.util.ContextEvaluateVisitor;
import astra.term.Term;

public class If extends AbstractStatement {
	Formula guard;
	Statement ifStatement, elseStatement;
	
	public If(String clazz, int[] data, Formula guard, Statement ifStatement, Statement elseStatement) {
		this.setLocation(clazz, data[0], data[1], data[2], data[3]);
		this.guard = guard;
		this.ifStatement = ifStatement;
		this.elseStatement = elseStatement;
	}
	
	public If(String clazz, int[] data, Formula guard, Statement ifStatement) {
		this.setLocation(clazz, data[0], data[1], data[2], data[3]);
		this.guard = guard;
		this.ifStatement = ifStatement;
	}

	@Override
	public StatementHandler getStatementHandler() {
		return new AbstractStatementHandler() {
			// int state = 0;
			@Override
			public boolean execute(Intention intention) {
				// if (state == 0) {
					try {
						// System.out.println("Before if...");
						Map<Integer, Term> bindings = intention.query((Formula) guard.accept(new ContextEvaluateVisitor(intention)));
						// System.out.println("After if: " + bindings);
						if (bindings == null) {
							if (elseStatement == null) return false;
							executor.addStatement(elseStatement.getStatementHandler());
						} else {
							executor.addStatement(ifStatement.getStatementHandler(), bindings);
						}
					} catch (Exception th) {
						intention.failed("Failure matching guard", th);
					}
				// 	state = 1;
				// 	return true;
				// }
				return false;
			}

			@Override
			public boolean onFail(Intention context) {
				return false;
			}

			@Override
			public Statement statement() {
				return If.this;
			}
			
			public String toString() {
				return "if (" + guard + ") ...";
			}
		};
	}

}
