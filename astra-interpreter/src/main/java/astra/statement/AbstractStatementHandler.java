package astra.statement;

import java.util.Queue;

import astra.core.RuleExecutor;
import astra.formula.Formula;
import astra.formula.Goal;

public abstract class AbstractStatementHandler implements StatementHandler {
	protected RuleExecutor executor;
	
	public void addGoals(Queue<Formula> list, Goal goal) {
		// Do nothing by default...
	}

	public void setRuleExecutor(RuleExecutor executor) {
		this.executor = executor;
	}
}
