package astra.statement;

import java.util.Queue;

import astra.core.Intention;
import astra.core.RuleExecutor;
import astra.formula.Formula;
import astra.formula.Goal;

public interface StatementHandler {
	boolean execute(Intention Intention);
	boolean onFail(Intention intention);
	Statement statement();
	void addGoals(Queue<Formula> list, Goal goal);
	void setRuleExecutor(RuleExecutor executor);
}
