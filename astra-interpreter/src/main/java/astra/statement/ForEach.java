package astra.statement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import astra.core.Intention;
import astra.formula.Formula;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.reasoner.util.VariableVisitor;
import astra.term.Term;
import astra.term.Variable;

public class ForEach extends AbstractStatement {
	Formula guard;
	Statement body;
	
	public ForEach(String clazz, int[] data, Formula guard, Statement body) {
		this.setLocation(clazz, data[0], data[1], data[2], data[3]);
		this.guard = guard;
		this.body = body;
	}
	
	@Override
	public StatementHandler getStatementHandler() {
		return new AbstractStatementHandler() {
			int state = 0;
			List<Map<Integer, Term>> results;
			VariableVisitor visitor;
			
			@Override
			public boolean execute(Intention intention) {
				switch(state) {
				case 0:
					Formula g =(Formula) guard.accept(new BindingsEvaluateVisitor(executor.getAllBindings(), intention.agent));
					visitor = new VariableVisitor();
					g.accept(visitor);
					try {
						results = intention.queryAll(g);
						if (results == null) return false;
						if (results.isEmpty()) {
							executor.addStatement(body.getStatementHandler(), new HashMap<Integer, Term>());
							state = 2;
						} else {
							executor.addStatement(body.getStatementHandler(), results.remove(0));
							state = results.isEmpty() ? 2:1;
		
						}
					} catch (Throwable th) {
						intention.failed("Failed to match guard", th);
					}
					return true;
				case 1:
					executor.addStatement(body.getStatementHandler(), results.remove(0));
					if (results.isEmpty()) state = 2;
					return true;
				case 2:
					for (Variable variable : visitor.variables()) {
						executor.removeVariable(variable);
					}
				}
				return false;
			}

			@Override
			public boolean onFail(Intention context) {
				return false;
			}

			@Override
			public Statement statement() {
				return ForEach.this;
			}
			
		};
	}

}
