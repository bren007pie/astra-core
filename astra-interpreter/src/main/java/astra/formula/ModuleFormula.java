package astra.formula;

import astra.reasoner.util.LogicVisitor;

public class ModuleFormula implements Formula {
	/**
	 *
	 */
	private static final long serialVersionUID = 5866231318281013321L;
	
	private String module;
	private Predicate predicate;
	private ModuleFormulaAdaptor adaptor;
	
	public ModuleFormula(String module, Predicate predicate, ModuleFormulaAdaptor adaptor) {
		this.module = module;
		this.predicate = predicate;
		this.adaptor = adaptor;
	}

	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public boolean matches(Formula formula) {
		return false;
	}

	public String module() {
		return module;
	}
	
	public Predicate predicate() {
		return predicate;
	}
	
	public ModuleFormulaAdaptor adaptor() {
		return adaptor;
	}
	
	public String toString() {
		return module + "." + predicate;
	}
}
