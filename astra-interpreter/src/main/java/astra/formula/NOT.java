package astra.formula;

import astra.reasoner.util.LogicVisitor;



public class NOT implements Formula {
	/**
	 *
	 */
	private static final long serialVersionUID = -2131745941801390044L;
	
	private Formula formula;
	
	public NOT(Formula formula) {
		this.formula = formula;
	}

	public Formula formula() {
		return formula;
	}

	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public boolean matches(Formula formula) {
		return (formula instanceof NOT) && ((NOT) formula).formula.matches(formula);
	}
	
	public String toString() {
		return "~" + formula;
	}
}
