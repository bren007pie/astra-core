package astra.formula;

import astra.reasoner.util.LogicVisitor;
import astra.term.Term;



public class IsNull implements Formula {
	/**
	 *
	 */
	private static final long serialVersionUID = -6283227105493295865L;
	private Term term;
	
	public IsNull(Term term) {
		this.term = term;
	}

	public Term formula() {
		return term;
	}

	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public boolean matches(Formula formula) {
		return (formula instanceof IsNull) && ((IsNull) formula).term.equals(term);
	}
}
