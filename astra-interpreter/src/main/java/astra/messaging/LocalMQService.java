package astra.messaging;


public class LocalMQService extends MessageService {
    private boolean logging = false;
    
	public static void launch() {
		MessageService.installService("local", new LocalMQService());
	}
	
	public boolean sendMessage(AstraMessage message) {
        if (logging) System.out.println("SENDING: " + message.performative + " from: " + message.sender + " to: " + message.receivers + " with: " + message.content);
		return this.receiveMessage(message);
	}

	@Override
	public void configure(String key, String value) {
        switch (key) {
            case "logging":
                logging = value.equals("on");
                break;
            default:
                System.err.println("[LocalMQService] Unknown propery: " + key);
        } 
	}

	@Override
	public void start() {}
}
