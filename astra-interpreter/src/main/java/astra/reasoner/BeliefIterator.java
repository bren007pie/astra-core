package astra.reasoner;

import java.util.Iterator;
import java.util.List;

import astra.formula.Formula;
import astra.formula.Predicate;

public class BeliefIterator implements Iterator<Formula> {
    private Queryable[] sources;
    Iterator<Formula> iterator = null;
    private Predicate target;
    private int index;

    public BeliefIterator(List<Queryable> sources, Predicate target) {
        this.sources = sources.toArray(new Queryable[sources.size()]);
        this.target = target;
        index = 0;
        step();
    }

    private void step() {
        while (index < sources.length && (iterator==null || !iterator.hasNext())) {
            iterator = this.sources[index++].iterator(target);
        }
    }

    @Override
    public boolean hasNext() {
        return iterator != null && iterator.hasNext();
    }

    @Override
    public Formula next() {
        Formula formula = iterator.next();
        step();
        return formula;
    }
}
