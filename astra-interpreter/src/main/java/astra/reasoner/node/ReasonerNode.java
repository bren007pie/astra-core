package astra.reasoner.node;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import astra.reasoner.Reasoner;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.term.Term;

public abstract class ReasonerNode {
    public static final int MAX_DEPTH = 50; // TODO: (Rem Collier) MAKE BIGGER...

    protected ReasonerNode parent;
    protected List<ReasonerNode> children = new ArrayList<>();
    protected List<Map<Integer, Term>> solutions = new LinkedList<>();
    protected boolean singleResult = false;
    protected int depth;
    protected boolean finished = false;
    protected boolean failed = false;
    protected Map<Integer, Term> initial;
    protected BindingsEvaluateVisitor visitor;


    public ReasonerNode(ReasonerNode parent, boolean singleResult) {
        this.parent = parent;
        this.singleResult = singleResult;
        depth = (parent == null) ? 0:parent.depth+1;
    }

    public ReasonerNode initialize(Reasoner reasoner) {
        return this;
    }

    public abstract boolean solve(Reasoner reasoner, Stack<ReasonerNode> stack);
    
    public boolean failed(Reasoner reasoner, Stack<ReasonerNode> stack) {
        if (parent != null) {
            stack.pop();
            return true;
        }
        return false;
    }
    public boolean resolve(Reasoner reasoner, Stack<ReasonerNode> stack) {
        if (isMaxDepth()) {
            // System.out.println("++++ MAX DEPTH ++++");
            finished = true;
            failed = true;
            return false;
        }

        return solve(reasoner, stack);
    }

    public boolean isFinished() {
        return finished;
    }

    public boolean isMaxDepth() {
        return depth == MAX_DEPTH;
    }

    public boolean isSingleResult() {
        return singleResult;
    }

    public boolean isFailed() {
        return failed;
    }

    public List<Map<Integer, Term>> solutions() {
        return solutions;
    }
}
