package astra.reasoner.node;

import java.util.Map;
import java.util.Stack;

import astra.formula.BracketFormula;
import astra.reasoner.NewReasoner;
import astra.reasoner.Reasoner;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.term.Term;

public class BracketReasonerNode extends ReasonerNode {
    ReasonerNode node;
    BracketFormula formula;
    int state = 0;

    public BracketReasonerNode(ReasonerNode parent, BracketFormula formula, Map<Integer, Term> initial, boolean singleResult) {
        super(parent, singleResult);

        this.formula = formula;
        this.initial = initial;
    }

    @Override
    public ReasonerNode initialize(Reasoner reasoner) {
        visitor = new BindingsEvaluateVisitor(initial, reasoner.agent());
        formula = (BracketFormula) formula.accept(visitor);
        return super.initialize(reasoner);
    }

    @Override
    public boolean solve(Reasoner reasoner, Stack<ReasonerNode> stack) {
        switch (state) {
            case 0:
                node = ((NewReasoner) reasoner).createReasonerNode(this, formula.formula(), initial, singleResult);
                stack.push(node);
                state++;
                return true;
            case 1:
                finished = node.finished;
                solutions = node.solutions;
                failed = node.failed;
                return !failed;
        }
        return false;
    }
}
