package astra.reasoner.node;

import java.util.Map;

import astra.formula.Formula;
import astra.term.Term;

public interface ReasonerNodeFactory<T extends Formula> {
	ReasonerNode create(T formula, Map<Integer, Term> bindings, boolean singleResult);
	ReasonerNode create(ReasonerNode node, T formula, Map<Integer, Term> bindings, boolean singleResult);
    
}
