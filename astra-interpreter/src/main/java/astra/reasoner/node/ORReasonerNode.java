package astra.reasoner.node;

import java.util.Map;
import java.util.Stack;

import astra.formula.OR;
import astra.reasoner.NewReasoner;
import astra.reasoner.Reasoner;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.term.Term;

public class ORReasonerNode extends ReasonerNode {
    ReasonerNode left, right;

    OR or;
    int state = 0;

    public ORReasonerNode(ReasonerNode parent, OR or, Map<Integer, Term> initial, boolean singleResult) {
        super(parent, singleResult);

        this.or = or;
        this.initial = initial;
    }

    @Override
    public ReasonerNode initialize(Reasoner reasoner) {
        visitor = new BindingsEvaluateVisitor(initial, reasoner.agent());
        or = (OR) or.accept(visitor);
        return super.initialize(reasoner);
    }

    @Override
    public boolean solve(Reasoner reasoner, Stack<ReasonerNode> stack) {
        // System.out.println("state = " + state);
        switch (state) {
            case 0:
                left = ((NewReasoner) reasoner).createReasonerNode(this, or.left(), initial, true);
                left.singleResult = false;
                state++;
            case 1:
                // System.out.println("left checking: " + left);
                stack.push(left);
                state++;
                return true;
            case 2:
                // System.out.println("failed? " + left.isFailed());
                if (!left.isFailed()) {
                    if (singleResult) {
                        solutions.add(left.solutions.isEmpty()? initial:left.solutions.get(0));
                    } else {
                        solutions = left.solutions;
                    }
                    finished = true;
                    // System.out.println("left passed: " + solutions);
                    return true;
                }
                
                right = ((NewReasoner) reasoner).createReasonerNode(this, or.right(), initial, true);
                right.singleResult = false;
                // System.out.println("right checking: " + right);

                state++;
            case 3:
                stack.push(right);
                state++;
                // System.out.println("here: " + state);
                return true;
            case 4:
                if (right.isFailed()) {
                    // System.out.println("right failed!");
                    finished = true;
                    failed = true;
                    return false;
                }

                if (singleResult) {
                    solutions.add(right.solutions.isEmpty()? initial:right.solutions.get(0));
                } else {
                    solutions = right.solutions;
                }

                // System.out.println("right passed: " + solutions);
                finished = true;
                return true;
        }
        return false;
    }

    
}
