package astra.reasoner.node;

import java.util.Map;
import java.util.Stack;

import astra.formula.Bind;
import astra.reasoner.Reasoner;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.term.Term;

public class BindReasonerNode extends ReasonerNode {
    Bind bind;

    public BindReasonerNode(ReasonerNode parent, Bind bind, Map<Integer, Term> initial, boolean singleResult) {
        super(parent, singleResult);

        this.bind = bind;
        this.initial = initial;
    }

    @Override
    public boolean solve(Reasoner reasoner, Stack<ReasonerNode> stack) {
        visitor = new BindingsEvaluateVisitor(initial, reasoner.agent());
        initial.put(bind.variable().id(), (Term) bind.accept(visitor));
        solutions.add(initial);
        finished = true;
        return true;
    }
}
