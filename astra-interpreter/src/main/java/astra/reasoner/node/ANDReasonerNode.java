package astra.reasoner.node;

import java.util.List;
import java.util.Map;
import java.util.Stack;

import astra.formula.AND;
import astra.reasoner.NewReasoner;
import astra.reasoner.Reasoner;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.term.Term;

public class ANDReasonerNode extends ReasonerNode {
    ReasonerNode left, right;
    int count  = 0;
    AND and;
    int state = 0;
    List<Map<Integer, Term>> options;
    int successes = 0;

    public ANDReasonerNode(ReasonerNode parent, AND and, Map<Integer, Term> initial, boolean singleResult) {
        super(parent, singleResult);

        this.and = and;
        this.initial = initial;
    }

    @Override
    public ReasonerNode initialize(Reasoner reasoner) {
        visitor = new BindingsEvaluateVisitor(initial, reasoner.agent());
        and = (AND) and.accept(visitor);
        // System.out.println("[AND] " + and);
        return super.initialize(reasoner);
    }

    @Override
    public boolean solve(Reasoner reasoner, Stack<ReasonerNode> stack) {
        if (count++ == 10000) {
            System.out.println("FORCED STOP: AND");
            System.exit(0);
        }
        // System.out.println("[AND] solve called: "  + count);

        switch (state) {
            case 0:
                left = ((NewReasoner) reasoner).createReasonerNode(this, and.formulae[0], initial, true);
                // Need to get all the left branches of the tree...
                // System.out.println("[AND] left: " + left);
                left.singleResult = false;
                state++;
            case 1:
                stack.push(left);
                state++;
                return true;
            case 2:
                // System.out.println("[AND] handling left...");
                // If the left child has failed, then this node has failed..
                if (left.isFailed()) {
                    // System.out.println("[AND] failed: " + left);
                    finished = true;
                    failed = true;
                    return false;
                }

                // If there area no solutions, then apply only the initial substitutions to the right node
                // otherwise apply the first of the left node substitutions to the right node...
                options = left.solutions;
                // System.out.println("[AND] left solutions: " + options);
                if (options.isEmpty()) {
                    right = ((NewReasoner) reasoner).createReasonerNode(this, and.formulae[1], initial, true);
                } else {
                    right = ((NewReasoner) reasoner).createReasonerNode(this, and.formulae[1], options.remove(0), true);
                }
                // System.out.println("[AND] first right: " + right);
                state++;
            case 3:
                stack.push(right);
                state++;
                return true;
            case 4:
                // System.out.println("[AND] " + right + " = " + right.isFailed());
                // If the right child has failed and there are no more options...
                if (!right.isFailed()) {
                    successes++;
                    solutions.add(right.solutions.isEmpty() ? initial:right.solutions.get(0)); 
                    // System.out.println("\tadding: "+ right.solutions);
                    // Have a solution...
                    if (singleResult) {
                        // If we only needed one, stop here...
                        finished = true;
                        return true;
                    }
                }

                if (options.isEmpty()) {
                    // We have no more options, so end here...
                    // System.out.println("\tFinished: " + solutions);
                    finished = true;
                    failed = (successes == 0);
                    return !failed;
                }

                // Try the next option...
                right = ((NewReasoner) reasoner).createReasonerNode(this, and.formulae[1], options.remove(0), true);
                // System.out.println("[AND] next right: " + right);

                stack.push(right);
                state = 4; 
                return true;                   
        }
        return false;
    }

    
}
