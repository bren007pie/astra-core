package astra.reasoner.node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import astra.formula.Formula;
import astra.formula.Inference;
import astra.formula.Predicate;
import astra.reasoner.BeliefIterator;
import astra.reasoner.NewReasoner;
import astra.reasoner.Reasoner;
import astra.reasoner.Unifier;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.reasoner.util.RenameVisitor;
import astra.reasoner.util.Utilities;
import astra.reasoner.util.VariableVisitor;
import astra.term.Term;

public class PredicateReasonerNode extends ReasonerNode {
    Predicate predicate;
    List<Formula> options = new ArrayList<Formula>();
    BeliefIterator beliefIterator;
    int index = 0;
    static int counter = 0;
    int state = 0;
    ReasonerNode node;
    VariableVisitor variableVisitor;
    Map<Integer, Term> bindings;
    int successes = 0;

    public PredicateReasonerNode(ReasonerNode parent, Predicate predicate, Map<Integer, Term> initial, boolean singleResult) {
        super(parent, singleResult);

        this.predicate = predicate;
        this.initial = initial;
    }

    @Override
    public ReasonerNode initialize(Reasoner reasoner) {
		visitor = new BindingsEvaluateVisitor(initial, reasoner.agent());
        predicate = (Predicate) predicate.accept(visitor);
        // System.out.println("Predicate: " + predicate);

        beliefIterator = new BeliefIterator(reasoner.sources(), predicate);

        return super.initialize(reasoner);
    }

    @Override
    public boolean solve(Reasoner reasoner, Stack<ReasonerNode> stack) {
        if (predicate.equals(Predicate.FALSE)) {
            finished = true;
            failed=true;
            return false;
        }
        
		if (predicate.equals(Predicate.TRUE)) {
            solutions.add(initial);
            finished = true;
			return true;
		}

       switch (state) {
            case 0:
                if (!beliefIterator.hasNext()) {
                    finished = true;
                    // System.out.println("Finished [" + predicate+"," + (successes > 0) + "]: " + solutions);
                    if (successes == 0) {
                        failed = true;
                        return false;
                    }
                    return true;
                    // return !(failed = solutions.isEmpty());
                }

                Formula formula = beliefIterator.next();
                // System.out.println("formula:"  + formula);
                if (Predicate.class.isInstance(formula)) {
                    Map<Integer, Term> bindings = Unifier.unify(predicate, (Predicate) formula.accept(visitor), new HashMap<Integer, Term>(initial), reasoner.agent());
                    if (bindings == null) return true;

                    // System.out.println("binding: " + bindings);
                    successes++;
                    solutions.add(bindings);
                    if (singleResult) finished = true;
                    return true;
                } else if (Inference.class.isInstance(formula)) {
                    Inference inference = (Inference) formula;
                    RenameVisitor renameVisitor = new RenameVisitor("rn_" + (counter++) + "_");
                    inference = (Inference) inference.accept(renameVisitor);

                    bindings = Unifier.unify(predicate, inference.head(), new HashMap<Integer, Term>(initial), reasoner.agent());
                    if (bindings == null) return true;

                    // get the core variables from the predicate
                    variableVisitor = new VariableVisitor();
                    predicate.accept(variableVisitor);

                    // Apply the bindings to the body and apply resolution...
                    Formula target = (Formula) inference.body().accept(new BindingsEvaluateVisitor(bindings, reasoner.agent()));
                    node = ((NewReasoner) reasoner).createReasonerNode(this, target, initial, true);
                    stack.add(node);
                    // System.out.println("Added: " + target);
                    state = 1;
                    return true;
                }
                break;
            case 1:
                // System.out.println("Failed? " + node.isFailed());
                if (node.isFailed()) {
                    // if it failed, try the next one...
                    state = 0;
                    return true;
                }

                successes++;
                for (Map<Integer, Term> b : node.solutions) {
                    // System.out.println("b=" + b);
                    // System.out.println("bindings: " + bindings);
                    solutions.add(Utilities.filter(Utilities.mgu(Utilities.merge(bindings, b)), variableVisitor.variables()));
                    if (singleResult) {
                        finished = true;
                        return true;
                    }                            
                }
                state = 0;
                return true;
        }

        return true;
    }
    
    public String toString() {
        return predicate.toString();
    }
}
