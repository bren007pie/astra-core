package astra.reasoner.node;

import java.util.Map;
import java.util.Stack;

import astra.formula.Comparison;
import astra.formula.Formula;
import astra.formula.Predicate;
import astra.reasoner.Reasoner;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.term.Term;

public class ComparisonReasonerNode extends ReasonerNode {
    Comparison comparison;
    Predicate result;

    public ComparisonReasonerNode(ReasonerNode parent, Comparison comparison, Map<Integer, Term> initial, boolean singleResult) {
        super(parent, singleResult);

        this.comparison = comparison;
        this.initial = initial;
    }

    @Override
    public boolean solve(Reasoner reasoner, Stack<ReasonerNode> stack) {
        visitor = new BindingsEvaluateVisitor(initial, reasoner.agent());
        Formula result = (Formula) comparison.accept(visitor);
        failed = result != Predicate.TRUE;

        if (!failed) {
            solutions.add(initial);
        }
        
        finished = true;
		return !failed;
    }
}
