package astra.reasoner.node;

import java.util.Map;
import java.util.Stack;

import astra.formula.NOT;
import astra.reasoner.NewReasoner;
import astra.reasoner.Reasoner;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.term.Term;

public class NOTReasonerNode extends ReasonerNode {
    NOT not;

    public NOTReasonerNode(ReasonerNode parent, NOT not, Map<Integer, Term> initial, boolean singleResult) {
        super(parent, singleResult);

        this.not = not;
        this.initial = initial;
    }

    @Override
    public ReasonerNode initialize(Reasoner reasoner) {
        visitor = new BindingsEvaluateVisitor(initial, reasoner.agent());
        not = (NOT) not.accept(visitor);
        // System.out.println("[NOT] " + not);
        return super.initialize(reasoner);
    }

    ReasonerNode node;
    int state = 0;
    @Override
    public boolean solve(Reasoner reasoner, Stack<ReasonerNode> stack) {
        switch (state) {
            case 0:
                node = ((NewReasoner) reasoner).createReasonerNode(this, not.formula(), initial, true);
                stack.add(node);
                state++;
                return true;
            case 1:
                failed = !node.isFailed();
                solutions.add(initial);
                finished = true;
                return !failed;

        }

        // List<Map<Integer, Term>> list = reasoner.query(not.formula());
        // failed = list == null;
        // System.out.println("[NOT] " + not + ","+ failed+" / list: " + list);
        // if (!failed) {
        //     solutions.add(initial);
        // }
        // finished = true;
        // return !failed;
        return false;
    }

    @Override
    public String toString() {
        return not.toString();
    }
}
