package astra.reasoner.node;

import java.util.Map;

import astra.formula.NOT;
import astra.term.Term;

public class NOTReasonerNodeFactory implements ReasonerNodeFactory<NOT> {

    @Override
    public ReasonerNode create(NOT formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new NOTReasonerNode(null, formula, bindings, singleResult);
    }
    
    @Override
    public ReasonerNode create(ReasonerNode parent, NOT formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new NOTReasonerNode(parent, formula, bindings, singleResult);
    }
    
}
