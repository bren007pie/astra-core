package astra.reasoner.node;

import java.util.Map;

import astra.formula.Bind;
import astra.term.Term;

public class BindReasonerNodeFactory implements ReasonerNodeFactory<Bind> {

    @Override
    public ReasonerNode create(Bind formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new BindReasonerNode(null, formula, bindings, singleResult);
    }
    
    @Override
    public ReasonerNode create(ReasonerNode parent, Bind formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new BindReasonerNode(parent, formula, bindings, singleResult);
    }
    
}
