package astra.reasoner.node;

import java.util.Map;

import astra.formula.OR;
import astra.term.Term;

public class ORReasonerNodeFactory implements ReasonerNodeFactory<OR> {

    @Override
    public ReasonerNode create(OR formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new ORReasonerNode(null, formula, bindings, singleResult);
    }
    
    @Override
    public ReasonerNode create(ReasonerNode parent, OR formula, Map<Integer, Term> bindings, boolean singleResult) {
        return new ORReasonerNode(parent, formula, bindings, singleResult);
    }
    
}
