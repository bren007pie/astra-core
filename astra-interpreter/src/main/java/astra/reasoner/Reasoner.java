package astra.reasoner;

import java.util.List;
import java.util.Map;

import astra.core.Agent;
import astra.formula.Formula;
import astra.term.Term;

/**
 * Core Interface for logical reasoning systems that may be attached to an agent. Sources
 * contain logic databases that are queried by the reasoner. All sources must implement the
 * {@link Queryable} interface.
 * 
 * @author rem
 *
 */
public interface Reasoner {
	void addSource(Queryable source);
	List<Map<Integer, Term>> queryAll(Formula formula);
	List<Map<Integer, Term>> query(Formula formula);
	List<Map<Integer, Term>> query(Formula formula, Map<Integer, Term> bindings);
	Agent agent();
	List<Queryable> sources();
}
