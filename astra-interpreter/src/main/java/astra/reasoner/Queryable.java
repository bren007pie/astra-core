package astra.reasoner;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import astra.formula.Formula;

/**
 * A Source for {@link Reasoner} implementations.  This interface defines a single method that
 * should return formulae matching the given formula.
 * 
 * @author rem
 */
public interface Queryable {
	static List<Formula> EMPTY_LIST = new LinkedList<>();
	
	void addMatchingFormulae(Queue<Formula> list, Formula formula);

	Iterator<Formula> iterator(Formula target);
}
