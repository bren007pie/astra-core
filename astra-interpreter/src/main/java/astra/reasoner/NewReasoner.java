package astra.reasoner;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import astra.core.Agent;
import astra.formula.AND;
import astra.formula.Bind;
import astra.formula.BracketFormula;
import astra.formula.Comparison;
import astra.formula.Formula;
import astra.formula.IsDone;
import astra.formula.ModuleFormula;
import astra.formula.NOT;
import astra.formula.OR;
import astra.formula.Predicate;
import astra.reasoner.node.ANDReasonerNodeFactory;
import astra.reasoner.node.BindReasonerNodeFactory;
import astra.reasoner.node.BracketReasonerNodeFactory;
import astra.reasoner.node.ComparisonReasonerNodeFactory;
import astra.reasoner.node.IsDoneReasonerNodeFactory;
import astra.reasoner.node.ModuleFormulaReasonerNodeFactory;
import astra.reasoner.node.NOTReasonerNodeFactory;
import astra.reasoner.node.ORReasonerNodeFactory;
import astra.reasoner.node.PredicateReasonerNodeFactory;
import astra.reasoner.node.ReasonerNode;
import astra.reasoner.node.ReasonerNodeFactory;
import astra.term.Term;

public class NewReasoner implements Reasoner {
    public static Map<Class<? extends Formula>, ReasonerNodeFactory<? extends Formula>> factories = new HashMap<>();
    static {
        factories.put(Predicate.class, new PredicateReasonerNodeFactory());
        factories.put(AND.class, new ANDReasonerNodeFactory());
        factories.put(OR.class, new ORReasonerNodeFactory());
        factories.put(NOT.class, new NOTReasonerNodeFactory());
        factories.put(BracketFormula.class, new BracketReasonerNodeFactory());
        factories.put(Comparison.class, new ComparisonReasonerNodeFactory());
        // factories.put(AcreFormula.class, new AcreFormulaReasonerNodeFactory());
        factories.put(IsDone.class, new IsDoneReasonerNodeFactory());
        factories.put(Bind.class, new BindReasonerNodeFactory());
        factories.put(ModuleFormula.class, new ModuleFormulaReasonerNodeFactory());
    }

    List<Queryable> sources = new LinkedList<Queryable>();
    Agent agent;

    public NewReasoner(Agent agent) {
        this.agent = agent;
    }
    
    @Override
    public void addSource(Queryable source) {
        sources.add(source);
    }

    @Override
    public List<Map<Integer, Term>> queryAll(Formula formula) {
        return doQuery(createReasonerNode(formula, false));
    }

    @Override
    public List<Map<Integer, Term>> query(Formula formula) {
        return doQuery(createReasonerNode(formula, true));
    }

    private List<Map<Integer, Term>> doQuery(ReasonerNode root) {
        Stack<ReasonerNode> stack = new Stack<>();
        stack.push(root);

        boolean success = true;
        while (success && !root.isFinished()) {
            ReasonerNode node = stack.peek();
            while (success && !node.isFinished()) {
                success = node.resolve(this, stack);
                if (node.isFailed()) {
                    success = node.failed(this, stack);
                }
                node = stack.peek();
            }
            stack.pop();
        }

        return success ? root.solutions():null;
    }

    @Override
    public List<Map<Integer, Term>> query(Formula formula, Map<Integer, Term> bindings) {
        return doQuery(createReasonerNode(formula, bindings, true));
    }

    private ReasonerNode createReasonerNode(Formula formula, boolean singleResult) {
        return createReasonerNode(null, formula, new HashMap<Integer, Term>(), singleResult);
    }
    
    private ReasonerNode createReasonerNode(Formula formula, Map<Integer, Term> bindings, boolean singleResult) {
        return createReasonerNode(null, formula, new HashMap<Integer, Term>(bindings), singleResult);
    }
    
	@SuppressWarnings({ "unchecked", "rawtypes" })
    public ReasonerNode createReasonerNode(ReasonerNode parent, Formula formula, Map<Integer, Term> bindings, boolean singleResult) {
        ReasonerNodeFactory factory = factories.get(formula.getClass());
        return factory.create(parent, formula, bindings, singleResult).initialize(this);
    }

	public Agent agent() {
		return agent;
    }
    
	public List<Queryable> sources() {
		return sources;
    }
    
	public static void main(String[] args) {
    }
}
