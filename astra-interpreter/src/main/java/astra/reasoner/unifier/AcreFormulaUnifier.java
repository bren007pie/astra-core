package astra.reasoner.unifier;

import java.util.Map;

import astra.core.Agent;
import astra.formula.AcreFormula;
import astra.formula.Formula;
import astra.reasoner.FormulaUnifier;
import astra.reasoner.Unifier;
import astra.term.Term;

public class AcreFormulaUnifier implements FormulaUnifier  {

	@Override
	public boolean isApplicable(Formula source, Formula target) {
		return (source instanceof AcreFormula) && (target instanceof AcreFormula);
	}

	@Override
	public Map<Integer, Term> unify(Formula source, Formula target, Map<Integer, Term> bindings, Agent agent) {
		AcreFormula s = (AcreFormula) source;
		AcreFormula t = (AcreFormula) target;
		
		Map<Integer, Term> b = Unifier.unify(
			new Term[] { s.cid(), s.index(), s.type(), s.performative() },
			new Term[] { t.cid(), t.index(), t.type(), t.performative() },
			bindings, agent);
		return b != null ? Unifier.unify(s.content(), t.content(), b, agent):null;
	}
    
}
