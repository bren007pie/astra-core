package astra.reasoner.unifier;

import java.util.Map;

import astra.core.Agent;
import astra.formula.Formula;
import astra.formula.FormulaVariable;
import astra.reasoner.FormulaUnifier;
import astra.term.FormulaTerm;
import astra.term.Term;
import astra.term.Variable;

public class FormulaVariableUnifier implements FormulaUnifier {

	@Override
	public boolean isApplicable(Formula source, Formula target) {
		return (source instanceof FormulaVariable) || (target instanceof FormulaVariable);
	}

	@Override
	public Map<Integer, Term> unify(Formula source, Formula target, Map<Integer, Term> bindings, Agent agent) {
		if (source instanceof FormulaVariable) {
			Variable variable = ((FormulaVariable) source).variable();
			bindings.put(variable.id(), new FormulaTerm(target));
			return bindings;
		} else if (target instanceof FormulaVariable) {
			Variable variable = ((FormulaVariable) target).variable();
			bindings.put(variable.id(), new FormulaTerm(source));
			return bindings;
        }
		return null;
	}
    
}
