package astra.reasoner.unifier;

import java.util.Map;

import astra.core.Agent;
import astra.formula.AND;
import astra.formula.Formula;
import astra.reasoner.FormulaUnifier;
import astra.reasoner.Unifier;
import astra.reasoner.util.Utilities;
import astra.term.Term;

public class ANDUnifier implements FormulaUnifier {

	@Override
	public boolean isApplicable(Formula source, Formula target) {
        return (source instanceof AND) && (target instanceof AND);
    }

	@Override
	public Map<Integer, Term> unify(Formula source, Formula target, Map<Integer, Term> bindings, Agent agent) {
        System.out.println("AND Unifier check - is used");
        AND s = (AND) source;
        AND t = (AND) target;

        Map<Integer, Term> temp = Unifier.unify(s.left(), t.left(), bindings, agent);
        if (temp == null)
            return null;

        temp = Unifier.unify(s.right(), t.right(), Utilities.merge(temp, bindings), agent);
        if (temp == null)
            return null;

        return temp;
	}
    
}
