package astra.ast.type;

import astra.ast.core.IElement;
import astra.ast.core.IElementVisitor;
import astra.ast.core.IType;
import astra.ast.core.ParseException;

public class ObjectType implements IType {
	int type;
	String clazz;
	IElement parent;
	
	public ObjectType(int type, String clazz) {
		this.type = type;
		this.clazz = clazz;
	}

	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}

	public int type() {
		return type;
	}
	
	public String getClazz() {
		return clazz;
	}

	public String getSource() {
		return null;
	}

	public boolean equals(Object object) {
		if (object instanceof ObjectType) {
			return clazz.equals(((ObjectType) object).clazz);
		}
		return false;
	}

	public IElement[] getElements() {
		return new IElement[0];
	}

	public IElement getParent() {
		return parent;
	}

	public IElement setParent(IElement parent) {
		this.parent =parent;
		return this;
	}
	
	public int getBeginLine() {
		return parent.getBeginLine();
	}

	public int getBeginColumn() {
		return parent.getBeginColumn();
	}

	public int charStart() {
		return 0;
	}

	public int charEnd() {
		return 0;
	}
	
	public String toString() {
		return clazz;
	}
}
