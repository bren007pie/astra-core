package astra.ast.core;

public class NoSuchASTRAClassException extends RuntimeException {
	/**
	 *
	 */
	private static final long serialVersionUID = 458681102101062496L;

	public NoSuchASTRAClassException(String msg) {
		super(msg);
	}
}
