package astra.ast.core;

import astra.ast.definition.FormulaDefinition;
import astra.ast.element.FunctionElement;
import astra.ast.element.GRuleElement;
import astra.ast.element.InferenceElement;
import astra.ast.element.InitialElement;
import astra.ast.element.ModuleElement;
import astra.ast.element.PackageElement;
import astra.ast.element.RuleElement;
import astra.ast.element.TypesElement;
import astra.ast.event.MessageEvent;
import astra.ast.event.ModuleEvent;
import astra.ast.event.UpdateEvent;
import astra.ast.formula.AndFormula;
import astra.ast.formula.BindFormula;
import astra.ast.formula.BracketFormula;
import astra.ast.formula.ComparisonFormula;
import astra.ast.formula.FormulaVariable;
import astra.ast.formula.GoalFormula;
import astra.ast.formula.IsDoneFormula;
import astra.ast.formula.ModuleFormula;
import astra.ast.formula.NOTFormula;
import astra.ast.formula.OrFormula;
import astra.ast.formula.PredicateFormula;
import astra.ast.formula.ScopedGoalFormula;
import astra.ast.statement.*;
import astra.ast.term.*;
import astra.ast.tr.BlockAction;
import astra.ast.tr.CartagoAction;
import astra.ast.tr.FunctionCallAction;
import astra.ast.tr.TRAction;
import astra.ast.tr.TRModuleCallAction;
import astra.ast.tr.TRRuleElement;
import astra.ast.tr.UpdateAction;
import astra.ast.type.BasicType;
import astra.ast.type.ObjectType;

public interface IElementVisitor {
	// Elements
	Object visit(ASTRAClassElement element, Object data) throws ParseException;
	Object visit(PackageElement element, Object data) throws ParseException;
	Object visit(ImportElement element, Object data);
	Object visit(ClassDeclarationElement element, Object data) throws ParseException;
	Object visit(InitialElement element, Object data) throws ParseException;
	Object visit(ModuleElement element, Object data) throws ParseException;
	Object visit(RuleElement element, Object data) throws ParseException;
	Object visit(InferenceElement element, Object data) throws ParseException;
	Object visit(FunctionElement element, Object data) throws ParseException;
	Object visit(TypesElement element, Object data) throws ParseException;
	Object visit(GRuleElement element, Object data) throws ParseException;
	
	// TR Components
	Object visit(TRRuleElement element, Object data) throws ParseException;
	Object visit(TRModuleCallAction action, Object data) throws ParseException;
	Object visit(CartagoAction action, Object data) throws ParseException;
	Object visit(TRAction action, Object data) throws ParseException;
	Object visit(BlockAction action, Object data) throws ParseException;
	Object visit(FunctionCallAction action, Object data) throws ParseException;
	Object visit(UpdateAction updateAction, Object data) throws ParseException;
	
	// Events
	Object visit(UpdateEvent event, Object data) throws ParseException;
	Object visit(MessageEvent event, Object data) throws ParseException;
	Object visit(ModuleEvent moduleEvent, Object data) throws ParseException;
	
	// Formulae
	Object visit(GoalFormula formula, Object data) throws ParseException;
	Object visit(PredicateFormula formula, Object data) throws ParseException;
	Object visit(NOTFormula formula, Object data) throws ParseException;
	Object visit(ComparisonFormula formula, Object data) throws ParseException;
	Object visit(AndFormula formula, Object data) throws ParseException;
	Object visit(OrFormula formula, Object data) throws ParseException;
	Object visit(FormulaVariable formula, Object data) throws ParseException;
	Object visit(ModuleFormula formula, Object data) throws ParseException;
	Object visit(ScopedGoalFormula formula, Object data) throws ParseException;
	Object visit(BracketFormula formula, Object data) throws ParseException;

	// Statements
	Object visit(DeclarationStatement statement, Object data) throws ParseException;
	Object visit(AssignmentStatement statement, Object data) throws ParseException;
	Object visit(BlockStatement statement, Object data) throws ParseException;
	Object visit(ModuleCallStatement statement, Object data) throws ParseException;
	Object visit(PlanCallStatement statement, Object data) throws ParseException;
	Object visit(SendStatement statement, Object data) throws ParseException;
	Object visit(IfStatement statement, Object data) throws ParseException;
	Object visit(UpdateStatement statement, Object data) throws ParseException;
	Object visit(SpawnGoalStatement statement, Object data) throws ParseException;
	Object visit(SubGoalStatement statement, Object data) throws ParseException;
	Object visit(QueryStatement statement, Object data) throws ParseException;
	Object visit(WhileStatement statement, Object data) throws ParseException;
	Object visit(ForEachStatement statement, Object data) throws ParseException;
	Object visit(WaitStatement statement, Object data) throws ParseException;
	Object visit(TryRecoverStatement statement, Object data) throws ParseException;
	Object visit(TRStatement statement, Object data) throws ParseException;
	Object visit(SynchronizedBlockStatement statement, Object data) throws ParseException;
	Object visit(MaintainBlockStatement statement, Object data) throws ParseException;
	Object visit(ScopedStatement statement, Object data) throws ParseException;
	Object visit(ForAllStatement statement, Object data) throws ParseException;
	Object visit(PlusPlusStatement plusPlusStatement, Object data) throws ParseException;
	Object visit(MinusMinusStatement minusMinusStatement, Object data) throws ParseException;
	Object visit(DoneStatement doneStatement, Object data) throws ParseException;

	// Terms
	Object visit(InlineVariableDeclaration term, Object data) throws ParseException;
	Object visit(Literal term, Object data) throws ParseException;
	Object visit(Operator term, Object data) throws ParseException;
	Object visit(Variable term, Object data) throws ParseException;
	Object visit(ModuleTerm term, Object data) throws ParseException;
	Object visit(ListTerm listTerm, Object data) throws ParseException;
	Object visit(QueryTerm queryTerm, Object data) throws ParseException;
	Object visit(Brackets brackets, Object data) throws ParseException;
	Object visit(Function function, Object data) throws ParseException;

	// List Functionality
	Object visit(CountTerm countTerm, Object data) throws ParseException;
	Object visit(HeadTerm headTerm, Object data) throws ParseException;
	Object visit(TailTerm headTerm, Object data) throws ParseException;
	Object visit(ListSplitterTerm term, Object data) throws ParseException;
	Object visit(AtIndexTerm atIndexTerm, Object data) throws ParseException;
	
	// Types
	Object visit(BasicType basicType, Object data) throws ParseException;
	Object visit(ObjectType objectType, Object data) throws ParseException;
	
	// Ontology Stuff
	Object visit(FormulaDefinition formulaDefinition, Object data) throws ParseException;
	Object visit(BindFormula formula, Object data) throws ParseException;
	Object visit(IsDoneFormula isDoneFormula, Object data) throws ParseException;
	Object visit(CountFormulaeTerm formula, Object data) throws ParseException;
}
