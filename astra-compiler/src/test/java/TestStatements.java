import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import astra.ast.core.ADTTokenizer;
import astra.ast.core.ASTRAParser;
import astra.ast.core.IStatement;
import astra.ast.core.ParseException;
import astra.ast.core.Token;
import astra.ast.element.TypesElement;
import astra.ast.formula.AndFormula;
import astra.ast.formula.NOTFormula;
import astra.ast.statement.BlockStatement;
import astra.ast.statement.DeclarationStatement;
import astra.ast.statement.ModuleCallStatement;
import astra.ast.statement.WaitStatement;

public class TestStatements {
	private ASTRAParser parser;
	private ADTTokenizer tokenizer;

	private List<Token> setup(String input) throws ParseException {
        tokenizer = new ADTTokenizer(new ByteArrayInputStream(input.getBytes()));
        parser = new ASTRAParser(tokenizer);
		List<Token> list = new ArrayList<>();
		Token token = tokenizer.nextToken();
		while (token != Token.EOF_TOKEN) {
			list.add(token);
			token = tokenizer.nextToken();
		}
        return list;
    }

	@Test
	public void waitFormulaTest() throws ParseException {
		List<Token> tokens = setup("wait(happy())");
		IStatement term = parser.createStatement(tokens);
		assertEquals(WaitStatement.class, term.getClass());
		WaitStatement statement = (WaitStatement) term;
		assertNotNull(statement.guard());
		assertNull(statement.timeout());
	}

	@Test(expected = ParseException.class)
	public void doneErrorTest() throws ParseException {
		List<Token> tokens = setup("done(");
		parser.createStatement(tokens);
	}

	@Test
	public void doneOKTest() throws ParseException {
		List<Token> tokens = setup("done");
		parser.createStatement(tokens);
	}

	@Test(expected = ParseException.class)
	public void waitNoBracketTest() throws ParseException {
		List<Token> tokens = setup("wait(happy()");
		parser.createStatement(tokens);
	}

	@Test
	public void waitTimeoutTest() throws ParseException {
		List<Token> tokens = setup("wait(happy(), 1000)");
		IStatement term = parser.createStatement(tokens);
		assertEquals(WaitStatement.class, term.getClass());
		WaitStatement statement = (WaitStatement) term;
		assertNotNull(statement.guard());
		assertNotNull(statement.timeout());
	}

	@Test
	public void waitTrueTest() throws ParseException {
		List<Token> tokens = setup("wait(true, 1000)");
		IStatement term = parser.createStatement(tokens);
		assertEquals(WaitStatement.class, term.getClass());
		WaitStatement statement = (WaitStatement) term;
		assertNotNull(statement.guard());
		assertNotNull(statement.timeout());
	}

	@Test(expected = ParseException.class)
	public void waitTimeoutNoBracketTest() throws ParseException {
		List<Token> tokens = setup("wait(happy(), 1000");
		parser.createStatement(tokens);
	}

	@Test
	public void doubleAssignmentTest() throws ParseException {
		List<Token> tokens = setup("double density=0.8;\n");
		IStatement term = parser.createStatement(tokens);
		assertEquals(DeclarationStatement.class, term.getClass());
		DeclarationStatement statement = (DeclarationStatement) term;
		assertNotNull(statement.variable());
		assertNotNull(statement.term());
	}

	@Test
	public void doubleAssignmentBlockTest() throws ParseException {
		List<Token> tokens = setup("{double density=0.8;double minority=0.4}");
		IStatement term = parser.createStatement(tokens);
		assertEquals(BlockStatement.class, term.getClass());
	}

	@Test
	public void moduleCallAndModuleTest() throws ParseException {
		List<Token> tokens = setup("map.getTopVote(string winner);");
		IStatement term = parser.createStatement(tokens);
		assertEquals(ModuleCallStatement.class, term.getClass());
	}
	//

	@Test
	public void classNameTest() throws ParseException {
		List<Token> tokens = setup("JsonNode node = converter.parse(content);");
		IStatement term = parser.createStatement(tokens);
		assertEquals(DeclarationStatement.class, term.getClass());
		DeclarationStatement declaration = (DeclarationStatement) term;
		System.out.println("Type: " + declaration.type());
	}

	@Test
	public void negatedWaitTest() throws ParseException {
		List<Token> tokens = setup("wait(~is(\"happy\"));");
		IStatement term = parser.createStatement(tokens);
		assertEquals(WaitStatement.class, term.getClass());
		WaitStatement wait = (WaitStatement) term;
		assertEquals(NOTFormula.class, wait.guard().getClass());
	}

	@Test
	public void andWaitTest() throws ParseException {
		List<Token> tokens = setup("wait(is(\"fun\") & ~is(\"happy\"));");
		IStatement term = parser.createStatement(tokens);
		assertEquals(WaitStatement.class, term.getClass());
		WaitStatement wait = (WaitStatement) term;
		assertEquals(AndFormula.class, wait.guard().getClass());
	}

	@Test
	public void timeoutWaitTest() throws ParseException {
		List<Token> tokens = setup("wait(is(\"fun\") & ~is(\"happy\"), 100);");
		IStatement term = parser.createStatement(tokens);
		assertEquals(WaitStatement.class, term.getClass());
		WaitStatement wait = (WaitStatement) term;
		assertEquals(AndFormula.class, wait.guard().getClass());
	}

	@Test(expected = ParseException.class)
	public void tooManyArgumentsWaitTest() throws ParseException {
		List<Token> tokens = setup("wait(is(\"fun\") & ~is(\"happy\"), 100, 50);");
		IStatement term = parser.createStatement(tokens);
		assertEquals(WaitStatement.class, term.getClass());
		WaitStatement wait = (WaitStatement) term;
		assertEquals(AndFormula.class, wait.guard().getClass());
	}

	@Test
	public void ontologyTest() throws ParseException {
		List<Token> tokens = setup("xxxx { formula is(string); formula detection();");
		TypesElement element = parser.createTypes(tokens);
		System.out.println("element: " + element);
		assertEquals(2, element.definitions().length);
	}
}
