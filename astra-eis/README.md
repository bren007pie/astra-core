# astra-eis-maven

This is a maven artifact that contains the code necessary to link agents written 
in ASTRA to EIS environments.

Versioning for this project will match the EIS version to indicate the level of 
compliance.

To use this plugin, include the following in your project:

