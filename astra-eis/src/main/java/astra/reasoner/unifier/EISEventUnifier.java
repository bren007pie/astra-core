package astra.reasoner.unifier;

import java.util.HashMap;
import java.util.Map;

import astra.core.Agent;
import astra.eis.EISEvent;
import astra.reasoner.EventUnifier;
import astra.reasoner.Unifier;
import astra.term.Term;

public class EISEventUnifier implements EventUnifier<EISEvent> {
	public Map<Integer, Term> unify(EISEvent source, EISEvent target, Agent agent) {
		if (source.type() != target.type()) return null;
		
		Map<Integer, Term> bindings = new HashMap<Integer, Term>();
		if (source.type() != EISEvent.ENVIRONMENT) {
			bindings = Unifier.unify(new Term[] {source.entity(), source.content()}, new Term[] {target.entity(), target.content()}, bindings, agent);
		}

		return bindings;
	}

}
