package astra.reasoner;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

import astra.eis.EISFormula;
import astra.formula.Formula;
import astra.reasoner.node.ReasonerNode;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.term.Term;

public class EISFormulaNode extends ReasonerNode {
    EISFormula formula;
	Queue<Formula> options = new LinkedList<>();
    int state = 0;

    public EISFormulaNode(ReasonerNode parent, EISFormula formula, Map<Integer, Term> initial, boolean singleResult) {
        super(parent, singleResult);

        this.formula = formula;
        this.initial = initial;
    }

    @Override
    public ReasonerNode initialize(Reasoner reasoner) {
        visitor = new BindingsEvaluateVisitor(initial, reasoner.agent());
        formula = (EISFormula) formula.accept(visitor);
        for (Queryable source : reasoner.sources()) {
            source.addMatchingFormulae(options, formula);
        }
        return super.initialize(reasoner);
    }

    @Override
    public boolean solve(Reasoner reasoner, Stack<ReasonerNode> stack) {
        while (!options.isEmpty()) {
            Formula testFormula = options.poll();
            Map<Integer, Term> bindings = Unifier.unify(formula, (EISFormula) testFormula.accept(visitor), new HashMap<Integer, Term>(initial), reasoner.agent());
            if (bindings != null) {
                solutions.add(bindings);
                if (singleResult) {
                    finished = true;
                }
                return true;
            }
        }

        finished = true;
        return !(failed = solutions.isEmpty());
    }

    
}
