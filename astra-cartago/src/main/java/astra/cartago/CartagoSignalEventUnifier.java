package astra.cartago;

import java.util.HashMap;
import java.util.Map;

import astra.core.Agent;
import astra.reasoner.EventUnifier;
import astra.reasoner.Unifier;
import astra.term.Term;

public class CartagoSignalEventUnifier implements EventUnifier<CartagoSignalEvent> {
	@Override
	public Map<Integer, Term> unify(CartagoSignalEvent source, CartagoSignalEvent target, Agent agent) {
		return Unifier.unify(new Term[] {source.id(), source.content()}, new Term[] {target.id(), target.content()}, new HashMap<Integer, Term>(), agent);
	}
}
