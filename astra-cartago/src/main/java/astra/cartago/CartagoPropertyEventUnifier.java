package astra.cartago;

import java.util.HashMap;
import java.util.Map;

import astra.core.Agent;
import astra.reasoner.EventUnifier;
import astra.reasoner.Unifier;
import astra.term.Term;

public class CartagoPropertyEventUnifier implements EventUnifier<CartagoPropertyEvent> {

	@Override
	public Map<Integer, Term> unify(CartagoPropertyEvent source, CartagoPropertyEvent target, Agent agent) {
		return Unifier.unify(new Term[] {source.id(), source.content()}, new Term[] {target.id(), target.content()}, new HashMap<Integer, Term>(), agent);
	}

}
