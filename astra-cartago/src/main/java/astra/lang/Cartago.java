package astra.lang;

import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import astra.cartago.CartagoAPI;
import astra.cartago.CartagoProperty;
import astra.cartago.CartagoPropertyEvent;
import astra.cartago.CartagoPropertyEventUnifier;
import astra.cartago.CartagoSignalEvent;
import astra.cartago.CartagoSignalEventUnifier;
import astra.core.Intention;
import astra.core.Module;
import astra.event.Event;
import astra.formula.Formula;
import astra.formula.Predicate;
import astra.reasoner.CartagoPropertyNodeFactory;
import astra.reasoner.NewReasoner;
import astra.reasoner.Unifier;
import astra.reasoner.util.AbstractEvaluateVisitor;
import astra.reasoner.util.ContextEvaluateVisitor;
import astra.reasoner.util.LogicVisitor;
import astra.reasoner.util.RenameVisitor;
import astra.reasoner.util.VariableVisitor;
import astra.term.Funct;
import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;
import astra.term.Variable;
import cartago.ArtifactId;
import cartago.ArtifactObsProperty;
import cartago.CartagoException;
import cartago.CartagoService;
import cartago.Op;
import cartago.OpFeedbackParam;

/**
 * This API provides additional support for the CArtAgO integration.
 * 
 * <p>
 * CArtAgO is an environment infrastructure. Support for it is provided
 * as a core part of ASTRA in terms of custom events, statements and
 * formulae. This API provides additional support for deplyoing and
 * configuring CArtAgO environments.
 * </p>
 * 
 * @author Rem Collier
 *
 */
public class Cartago extends Module {
	private CartagoAPI cartagoAPI;

	static {
		Unifier.eventFactory.put(CartagoPropertyEvent.class, new CartagoPropertyEventUnifier());
		Unifier.eventFactory.put(CartagoSignalEvent.class, new CartagoSignalEventUnifier());

		// Need to fix
		NewReasoner.factories.put(CartagoProperty.class, new CartagoPropertyNodeFactory());

		AbstractEvaluateVisitor.addFormulaHandler(new AbstractEvaluateVisitor.Handler<CartagoProperty>() {
			public Class<CartagoProperty> getType() { return CartagoProperty.class; }
			public Object handle(LogicVisitor visitor, CartagoProperty property, boolean passByalue) {
				return new CartagoProperty((Predicate) property.content().accept(visitor));
			}
		});
		RenameVisitor.addFormulaHandler(new RenameVisitor.Handler<CartagoProperty>() {
			public Class<CartagoProperty> getType() { return CartagoProperty.class; }
			public Object handle(LogicVisitor visitor, CartagoProperty property, String modifier, Map<Integer, Term> bindings) {
				return new CartagoProperty((Predicate) property.content().accept(visitor));
			}
		});
		VariableVisitor.addFormulaHandler(new VariableVisitor.Handler<CartagoProperty>() {
			public Class<CartagoProperty> getType() { return CartagoProperty.class; }
			public Object handle(LogicVisitor visitor, CartagoProperty property, Set<Variable> variables) {
				property.content().accept(visitor);
				return null;
			}
		});
	}
	
	/**
	 * Action that starts a local CArtAgO node.
	 * 
	 * @return
	 */
	@ACTION	public boolean startService() {
		try {
			CartagoService.startNode();
		} catch (CartagoException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Action that registers the agent with the local CArtAgO node.
	 * 
	 * @return
	 */
	@ACTION	public boolean link() {
		cartagoAPI = CartagoAPI.create(agent);
		return true;
	}
	
	@EVENT( symbols={"+", "-"}, types = {"string", "funct" }, signature="$cpe" )
	public Event property(String symbol, Term id, Term args) {
		return new CartagoPropertyEvent(Primitive.newPrimitive(symbol), id, args);
	}

	@EVENT( symbols={}, types = {"string", "funct" }, signature="$cse" )
	public Event signal(Term id, Term args) {
		return new CartagoSignalEvent(id, args);
	}
	
	@TERM
	@SuppressWarnings("rawtypes")
	public Object[] params(ListTerm list) {
		Object[] array = new Object[list.size()];
		for (int i=0;i<array.length;i++) {
			Term term = list.get(i);
			if (term instanceof Primitive) {
				array[i] = ((Primitive) term).value();
			}
		}
		return array;
	}

	/**
	 * Use of SUPPRESS_NOTIFICATIONS annotation to stop the interpreter from
	 * notifying itself on completion of the action. For CArtAgO, notification is
	 * done asynchronously when the operation actually completes...
	 * 
	 *  This is handled within the {@link astra.cartago.CartagoAPI} class.
	 *  
	 * @param context
	 * @param action
	 * @return
	 * @throws CartagoException 
	 */
	@SUPPRESS_NOTIFICATIONS
	public boolean auto_action(Intention context, Predicate action) {
		Predicate activity = action;
		Op op = null;
		
		// Last argument is the operation
		if (action.predicate().equals("operation")) {
			Funct funct = (Funct) action.termAt(action.size()-1);
			activity = (Predicate) new Predicate(funct.functor(), funct.terms());
		}

		ContextEvaluateVisitor visitor = new ContextEvaluateVisitor(context, true);
		activity = (Predicate) activity.accept(visitor);
		LinkedList<Object> list = cartagoAPI.getArguments(activity);
		op = list.isEmpty() ? new Op(activity.predicate()):new Op(activity.predicate(), list.toArray());
		
		
		try {
			context.suspend();
			// System.out.println("Suspended: " + context.toString());
			if (action.predicate().equals("operation") && action.size() == 2) {
				// Assume the first argument is the artifact id
				Term term = (Term) action.termAt(0).accept(visitor);
				if (!Primitive.class.isInstance(term)) {
					throw new RuntimeException("Failed to bind ArtifactId for CArtAgO Operation: " + action);
				}
				
				// We have an artifact id...
				Object o = ((Primitive<?>) term).value();
				if (o instanceof ArtifactId) {
					cartagoAPI.doOperation((ArtifactId) o, op, context, activity);
				} else if (o instanceof String) {
					cartagoAPI.doOperation(o.toString(), op, context, activity);
				} else {
					throw new RuntimeException("Could not handle artifact id type: " + o.getClass().getName());
				}
			} else {
				cartagoAPI.doOperation(op, context, activity);
			}
			return true;
		} catch (CartagoException e) {
            e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	@FORMULA
	public Formula auto_formula(Predicate formula) {
		return new CartagoProperty(formula);
	}
	
	@FORMULA public Formula property(ArtifactId id, Funct formula) {
		return new CartagoProperty(id, new Predicate(formula.functor(), formula.terms()));
	}

	@TERM public Funct toFunction(ArtifactObsProperty prop) {
		Object[] values = prop.getValues();
		Term[] terms = new Term[values.length];
		for (int i=0;i<terms.length;i++) {
			if (OpFeedbackParam.class.isInstance(values[i])) {
				terms[i] = Primitive.newPrimitive(((OpFeedbackParam<?>) values[i]).get());
			} else {
				terms[i] = Primitive.newPrimitive(values[i]);
			}
		}
		return new Funct(prop.getName(), terms);
	}

	@ACTION public boolean dumpProperties() {
		System.out.println("DUMP OF CARTAGO PROPERTIES...");
		for (Entry<String, Set<String>> entry : cartagoAPI.store().getArtifactProperties()) {
			System.out.println("Artifact: " + entry.getKey());
			for(String value : entry.getValue()) {
				System.out.println("\t" + cartagoAPI.store().getAssociatedObservation(value));
			}
		}
		return true;
	}
}
