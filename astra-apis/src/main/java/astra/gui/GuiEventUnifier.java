package astra.gui;


import java.util.HashMap;
import java.util.Map;

import astra.core.Agent;
import astra.reasoner.EventUnifier;
import astra.reasoner.Unifier;
import astra.term.Term;

public class GuiEventUnifier implements EventUnifier<GuiEvent> {

	@Override
	public Map<Integer, Term> unify(GuiEvent source, GuiEvent target, Agent agent) {
		return Unifier.unify(
				new Term[] {source.type, source.args}, 
				new Term[] {target.type, target.args}, 
				new HashMap<Integer, Term>(),
				agent);
	}


}
