package astra.gui;

import astra.event.Event;
import astra.reasoner.util.LogicVisitor;
import astra.term.ListTerm;
import astra.term.Term;

public class GuiEvent implements Event {
	Term type;
	ListTerm args;
	
	public GuiEvent(Term type) {
		this.type = type;
		args = new ListTerm();
	}
	
	public GuiEvent(Term id, ListTerm args) {
		this.type = id;
		this.args = args;
	}

	public Object getSource() {
		return null;
	}

	public String signature() {
		return "$gui:";
	}

	public Event accept(LogicVisitor visitor) {
		return new GuiEvent((Term) type.accept(visitor), (ListTerm) args.accept(visitor));
	}
}
