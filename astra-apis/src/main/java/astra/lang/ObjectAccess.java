package astra.lang;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;

import astra.core.Module;
import astra.formula.Formula;
import astra.formula.Predicate;
import astra.term.Funct;
import astra.term.LangUtils;
import astra.term.ListTerm;
import astra.term.Primitive;

public class ObjectAccess extends Module {
    @TERM
    public Object create(String classname) {
        try {
            return Class.forName(classname).getConstructor().newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @FORMULA
    public Formula is(Object object, String fieldName, int value) {
        return getInt(object, fieldName) == value ? Predicate.TRUE : Predicate.FALSE;
    }

    @FORMULA
    public Formula is(Object object, String fieldName, long value) {
        return getLong(object, fieldName) == value ? Predicate.TRUE : Predicate.FALSE;
    }

    @FORMULA
    public Formula is(Object object, String fieldName, boolean value) {
        return getBoolean(object, fieldName) == value ? Predicate.TRUE : Predicate.FALSE;
    }

    @FORMULA
    public Formula is(Object object, String fieldName, String value) {
        return getString(object, fieldName) == value ? Predicate.TRUE : Predicate.FALSE;
    }

    @TERM
    public int getInt(Object object, String fieldName) {
        try {
            Field field = object.getClass().getField(fieldName);
            return ((Integer) field.get(object)).intValue();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @TERM
    public long getLong(Object object, String fieldName) {
        try {
            Field field = object.getClass().getField(fieldName);
            return ((Long) field.get(object)).longValue();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @TERM
    public boolean getBoolean(Object object, String fieldName) {
        try {
            Field field = object.getClass().getField(fieldName);
            return ((Boolean) field.get(object)).booleanValue();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @TERM
    public String getString(Object object, String fieldName) {
        try {
            Field field = object.getClass().getField(fieldName);
            return field.get(object).toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @ACTION
    public boolean set(Object object, String fieldName, int value) {
        try {
            Field field = object.getClass().getField(fieldName);
            field.set(object, value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @ACTION
    public boolean set(Object object, String fieldName, long value) {
        try {
            Field field = object.getClass().getField(fieldName);
            field.set(object, value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @ACTION
    public boolean set(Object object, String fieldName, boolean value) {
        try {
            Field field = object.getClass().getField(fieldName);
            field.set(object, value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @ACTION
    public boolean set(Object object, String fieldName, String value) {
        try {
            Field field = object.getClass().getField(fieldName);
            field.set(object, value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @TERM
    public ListTerm toList(Object object, String fieldName) {
        try {
            Field field = object.getClass().getField(fieldName);
            Object value = field.get(object);
            if (Collection.class.isAssignableFrom(value.getClass())) {
                ListTerm list = new ListTerm();
                ((Collection<?>) value).forEach(obj -> {
                    list.add(Primitive.newPrimitive(obj));
                });
                return list;
            }
            throw new RuntimeException("Field: '" + fieldName + "' does not exist for class: " + object.getClass().getCanonicalName());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @ACTION
    public boolean invoke(Object object, Funct call) {
        try {
            Method method = object.getClass().getMethod(call.functor());
            
            method.invoke(object, LangUtils.toArray(call.terms()));
            return true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @FORMULA
    public Formula isType(Object object, String type) {
        try {
            return Class.forName(type).isInstance(object) ? Predicate.TRUE:Predicate.FALSE;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @TERM
    public String type(Object object) {
        return object.getClass().getCanonicalName();
    }
}
