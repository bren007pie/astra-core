package astra.unit;
import astra.core.Module;
import java.util.Arrays;

public class UnitTest extends Module {
	public static final String SUCCESS_MESSAGE = "Success";
	@ACTION
	public boolean assertEquals(TestSuite suite, int first, int second) {
		if (first == second) {
			suite.testResult(first + " == " + second, true);
		} else {
			suite.testResult(first + " != " + second, false);
		}
		return true;
	}

	@ACTION
	public boolean assertEquals(TestSuite suite, long first, long second) {
		if (first == second) {
			suite.testResult(first + " == " + second, true);
		} else {
			suite.testResult(first + " != " + second, false);
		}
		return true;
	}

	@ACTION
	public boolean assertEquals(TestSuite suite, String first, String second) {
		if (first.equals(second)) {
			suite.testResult(first + " == " + second, true);
		} else {
			suite.testResult(first + " != " + second, false);
		}
		return true;
	}

	@ACTION
	public boolean assertArrayEquals (TestSuite suite, int[] first, int[] second) {

		if (Arrays.equals(first, second)) {

			suite.testResult("The two Arrays are equal", true);
		} else {

			suite.testResult("The two Arrays are not equal", false);
		}

		return true;
	}

	@ACTION
	public boolean assertArrayEquals (TestSuite suite, char[] first, char[] second) {

		if (Arrays.equals(first, second)) {

			suite.testResult("The two Arrays are equal", true);
		} else {

			suite.testResult("The two Arrays are not equal", false);
		}

		return true;
	}

	@ACTION
	public boolean assertTrue (TestSuite suite, String msg, boolean condition) {

		if (!condition) {

			fail (suite, msg);
		}

		return true;
	}

	@ACTION
	public boolean assertTrue (TestSuite suite, boolean condition) {

		return assertTrue(suite, null, condition);
	}

	@ACTION
	public boolean fail(TestSuite suite, String msg) {

		// if (fail == null) {

		// 	suite.testResult(null, false);
		// } else {

		// 	suite.testResult(msg, false);
		// }

		return true;
	}

	@ACTION
	public boolean fail(TestSuite suite) {

		return fail(suite, null);
	}

	@ACTION
	public boolean success(TestSuite suite) {
		suite.testResult(SUCCESS_MESSAGE, true);
		return true;
	}
}