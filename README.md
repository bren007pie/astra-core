# astra-project

This project contains 4 sub-projects that implement the core of the ASTRA programming language.  These include:

* `astra-interpeter`: The core interpreter that underpins ASTRA.  More information can be found [here](astra-interpreter/)
* `astra-apis`: A Library of APIs the come packaged with ASTRA.  More information can be found [here](astra-apis/)
* `astra-compiler`: The ASTRA compiler is responsible for converting ASTRA source code into Java classes that can be directly executed.  More information can be found [here](astra-compiler/)
* `astra-base`: A base project that contains the standard configuratio for an ASTRA project and which can be used as a parent project for applications.  More information can be found [here](astra-base/)
