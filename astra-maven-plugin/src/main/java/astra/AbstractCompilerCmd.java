package astra;

import java.io.File;
import java.util.List;

public abstract class AbstractCompilerCmd extends AbstractCmd {
	protected final List<String> classpath;
	
	public AbstractCompilerCmd(File baseDir, List<String> classpath) {
		super(baseDir);
		this.classpath = classpath;
	}
	
	protected String getClasspath() {
		StringBuffer buf = new StringBuffer();
		char delim = ':';
		if (System.getProperty("os.name").startsWith("Windows")) delim = ';';
		boolean first = true;
		for (String jar : classpath) {
			if (first) first = false; else buf.append(delim);
			buf.append(jar);
		}
		return buf.toString();
	}
}
