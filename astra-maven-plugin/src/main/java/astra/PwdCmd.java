package astra;

import java.io.File;

public class PwdCmd extends AbstractCmd {
	public PwdCmd(File baseDir) {
		super(baseDir);
	}
	
	public String[] getCommand() {
		return new String[] { "pwd" };
	}

}
