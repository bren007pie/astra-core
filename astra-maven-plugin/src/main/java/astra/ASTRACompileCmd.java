package astra;

import java.io.File;
import java.util.List;

public class ASTRACompileCmd extends AbstractCompilerCmd {
	private final String source;
	private final String target;

	public ASTRACompileCmd(File baseDir, String source, List<String> classpath) {
		this(baseDir, source, source, classpath);
	}

	public ASTRACompileCmd(File baseDir, String  source, String target, List<String> classpath) {
		super(baseDir, classpath);
		this.source = source;
		this.target = target;
		// System.out.println("BaseDir: " + baseDir.getAbsolutePath());
		// System.out.println("Classpath: " + getClasspath());
	}
	
	public String[] getCommand() {
		return new String[] { "java", "-cp", getClasspath(), "astra.compiler.ASTRACompiler", source, target };
	}
}
