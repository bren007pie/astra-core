package astra;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

@Mojo( name = "test",  defaultPhase = LifecyclePhase.TEST, threadSafe = true, requiresDependencyResolution = ResolutionScope.TEST)
public class TestRunnerMojo extends AbstractMojo {

	@Parameter(defaultValue = "${project}", required = true, readonly = true)
	private MavenProject project;

	@Parameter(defaultValue = "${session}", readonly = true)
	private MavenSession session;

	@Parameter(defaultValue = "target/gen/test/java", readonly = true)
	private String target;

	public void execute() throws MojoExecutionException, MojoFailureException {
		try {
			if (!new File(project.getBasedir(), target).exists()) {
				return;
			}

			List<String> classpath = project.getCompileClasspathElements();
			classpath.add(project.getBuild().getOutputDirectory());
			new ASTRARunTestCmd(new File(project.getBuild().getDirectory()), classpath).execute();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (DependencyResolutionRequiredException e) {
			e.printStackTrace();
		}
	}
}
