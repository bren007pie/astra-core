package astra;

import java.io.File;
import java.util.List;

public class ASTRARunTestCmd extends AbstractCompilerCmd {
    
	public ASTRARunTestCmd(File baseDir, List<String> classpath) {
		super(baseDir, classpath);
	}
	
	public String[] getCommand() {
		return new String[] { "java", "-cp", getClasspath(), "astra.unit.TestRunner"};
	}
}
