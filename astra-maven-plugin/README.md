# astra-maven-plugin

This project implements a maven plugin that can be used to compile, test, and run ASTRA agent programs. To use the plugin, you should include the following in your Maven Project File (pom.xml):

```
    <plugin>
        <groupId>com.astralanguage</groupId>
        <artifactId>astra-maven-plugin</artifactId>
        <version>1.0.0</version>
        <configuration>
            <mainClass>${astra.main}</mainClass>
            <mainName>${astra.name}</mainName>
        </configuration>
        <executions>
            <execution>
                <id>astra.compile</id>
                <phase>compile</phase>
                <goals>
                    <goal>compile</goal>
                </goals>
            </execution>
            <execution>
                <id>astra.test</id>
                <phase>test-compile</phase>
                <goals>
                    <goal>testCompile</goal>
                </goals>
            </execution>
            <execution>
                <id>astra.testrun</id>
                <phase>test</phase>
                <goals>
                    <goal>test</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
```

There are two properties that can be set for the plugin:

* `astra.main`: the name of the ASTRA program that should be run first (this defaults to "Main")
* `astra.name`: the name of the agent that is created (this defaults to "main")

## Usage:

Compilation of ASTRA code is integrated with the "compile" phase and can be invoked by typing:

```
$ mvn compile
```

Testing of ASTRA code is integrated with the "test" phase and can be invoked by typing:

```
$ mvn test
```

Running of ASTRA code is not integrated with the Jar lifecycle but can be invoked by typing:

```
$ mvn astra:deploy
```